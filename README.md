# GeoWeb Workshop

This project is intended to be used as part of the geoweb-core workshop as hosted on 8th of May 2020 by KNMI.

### Installation

To run the geoweb-workshop storybook or application you need npm. The easiest way to install npm is via nvm. Please visit https://nvm.sh/ and follow the instructions. When nvm is installed, please run the following command:

```
nvm install 8
```

### To run the storybook

```
npm ci
npm run storybook
```

### To run the application

```
npm ci
npm run start
```

### To build the application and serve the dist folder locally

```
npm ci
npm run build
npx serve dist
```

### Managing packages

Follow the instructions below for dependency management. Please refer to [the documentation](https://docs.npmjs.com/cli-documentation/) for more information on any of the specific commands.

#### Removing extraneous packages

During development, we may end up with packages that have been installed but are not actually being used. In order to remove them, run `npm prune`.

#### Adding a new dependency

Adding a new dependency can be done following these steps:

1. `npm install <package-name>` or `npm install <package-name> --save-dev`
2. commit the updated files package.json and package-lock.json

Make sure to add the package as a dev depencency when it's only used for development purposes (like storybook or linting).

#### Updating an existing dependency

To see a list of which packages could need updating, run `npm outdated -l`.
A red package name means there’s a newer version matching the semver requirements in the package.json, so it can be updated. Yellow indicates that there’s a newer version higher than the semver requirements in the package.json (usually a new major version), so proceed with caution.

To update a single package to its latest version that matches the version required in package.json (red in the list), run `npm update <package-name>`, and commit the updated package-lock.json file.

To update all packages at once (that are red in the list), run `npm update`, and commit the updated package-lock.json file.

To update a single package to a version higher than the required version in package.json (yellow in the list):

1. check if there are any breaking changes to be aware of in the new version
2. `npm install <package-name>@<version>` or `npm install <package-name>@<version> --save-dev`
3. commit the updated files package.json and package-lock.json
4. update the code related to breaking changes

After any version change, make sure to test if everything still works.

#### Checking for vulnerabilities

To scan the project for any known vulnerabilities in existing dependencies, you can run `npm audit`. To fix them:

1. `npm audit fix`
2. commit the updated package-lock.json
3. test if everything still works

### License

This project is licensed under the terms of the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license.
You may not use this file except in compliance with the License.
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
