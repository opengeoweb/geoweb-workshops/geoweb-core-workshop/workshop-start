/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import InfoIcon from '@material-ui/icons/Info';

import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Box,
  Typography,
} from '@material-ui/core';

export const InfoDialog: React.FC = () => {
  const [open, setOpen] = React.useState(false);

  const handleToggleDialog = (): void => {
    setOpen(!open);
  };

  return (
    <div>
      <Button style={{ color: 'white' }} onClick={handleToggleDialog}>
        Info&nbsp;
        <InfoIcon />{' '}
      </Button>
      <Dialog
        fullWidth
        maxWidth="sm"
        open={open}
        onClose={handleToggleDialog}
        aria-labelledby="form-dialog-title"
        style={{ zIndex: 10000 }}
      >
        <DialogTitle id="form-dialog-title">
          GeoWeb Workshop: 8th of May 2020
        </DialogTitle>
        <DialogContent>
          <DialogContentText>Useful links:</DialogContentText>
          <Box>
            <Typography variant="body1">
              -{' '}
              <a href="https://www.npmjs.com/package/@opengeoweb/core">
                {' '}
                npm package{' '}
              </a>
              <br />-{' '}
              <a href="https://gitlab.com/opengeoweb/geoweb-core">
                {' '}
                GeoWeb-Core repository{' '}
              </a>
              <br />-{' '}
              <a href="https://opengeoweb.gitlab.io/geoweb-core/docs/">
                {' '}
                API documentation{' '}
              </a>
            </Typography>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleToggleDialog} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
